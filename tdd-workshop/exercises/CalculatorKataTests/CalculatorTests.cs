﻿using CalculatorKata;
using Xunit;

namespace CalculatorKataTests
{
    public class CalculatorTests
    {
        // Look in `./readme.md` for the requirements.

        // Be sure to adhere to the cycle!

        // 1. Write a unit test
        // 2. See the test fail
        // 3. Write the **minimum** amount of code to make the test go green
        // 4. Refactor
        // 5. Repeat

        [Fact(DisplayName = "Calculator can handle no values")]
        public void TestRequirement1()
        {
            var subject = new Calculator();

            Assert.True(false);
        }
    }
}