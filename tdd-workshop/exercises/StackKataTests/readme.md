# StackKata

This kata will implement a Stack.

The stack must support the following:

- Size can be limited.
- An element can be added to the stack. (push)
- An element can be removed. (pop)
- Check what the last element on the stack was. (peek)
- Get the current size of the stack.
- Allow the Stack to accept any data type.
- Add exception handling when the size is exceeded. 
