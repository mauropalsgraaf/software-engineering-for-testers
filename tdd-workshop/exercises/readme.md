## TDD kata's

These kata's should help you understand the mindset of TDD.

They contain the following exercises (in order):

0. FizzBuzzKata
1. GreetingKata
2. CalculatorKata
3. StackKata
4. RomanNumeralsKata

When writing your tests keep the following principles in mind:

- Fast, Independent, Repeatable, Self validation, Timely
- Arrange, Act, Assert (Given When Then)
